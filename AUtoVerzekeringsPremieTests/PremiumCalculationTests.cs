﻿using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using System;

namespace AutoVerzekeringsPremieTests
{
    public class PremiumCalculationTests
    {
       [Theory]
        [InlineData(0456)]
        [InlineData(2500)]
        [InlineData(3550)]
        [InlineData(3700)]
        [InlineData(4500)]
        public void PolicyHolderPostalCodeIsCheckedAndCalculateRate(int PostalCode)
        {
            // Arrange 
            Vehicle vehicle = new Vehicle(80, 10000, 2020);
            var policyholderBase = new PolicyHolder(19, "10-10-2020", 9999, 0);
            var policyholder = new PolicyHolder(19, "10-10-2020", PostalCode, 0);
            PremiumCalculation premiumCalculationBase = new PremiumCalculation(vehicle, policyholderBase, InsuranceCoverage.WA);
            var vermeningvuldiging = (PostalCode >= 1000 && PostalCode < 3600) ? 1.05: (PostalCode >= 360 && PostalCode < 4500) ? 1.02: 1;
            var ExpectedValue = premiumCalculationBase.PremiumAmountPerYear * vermeningvuldiging;
            // Act
            
            PremiumCalculation premiumCalculation = new PremiumCalculation(vehicle, policyholder, InsuranceCoverage.WA);


            //Assert
            Assert.Equal(ExpectedValue, premiumCalculation.PremiumAmountPerYear);

        }

       
        
        [Fact]
        public void PremiumCalculationsCanCalculateTheBasePremium()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 10000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(30, "10-10-2000", 8888, 1);
            double expectedPremium = ((double)10000 / 100 - 1 + (double)80 / 5) / 3;

            // Act
            PremiumCalculation premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(expectedPremium, premiumCalculation.PremiumAmountPerYear);

        }

        [Fact]
        public void PremiumCalculationsWaPlusRaiseTest()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 10000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(30, "10-10-2000", 8888, 1);

            PremiumCalculation premiumWa = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);


            // Act
            PremiumCalculation premiumCalculationWaPlus = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA_PLUS);

            //Assert
            Assert.Equal(premiumWa.PremiumAmountPerYear * 1.2, premiumCalculationWaPlus.PremiumAmountPerYear);

        }

        [Fact]
        public void PremiumCalculationAllRiskRaiseTest()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 10000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(30, "10-10-2000", 8888, 1);

            PremiumCalculation premiumWa = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);


            // Act
            PremiumCalculation premiumCalculationAllRisk = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.ALL_RISK);

            //Assert
            Assert.Equal(premiumWa.PremiumAmountPerYear * 2, premiumCalculationAllRisk.PremiumAmountPerYear);

        }


        [Fact]
        public void PremiumCalculationsCanCalculateTheMonth()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 10000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(19, "10-10-2000", 8888, 0);

            var premiumCalculationsYear = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            // Act
            var premiumCalculationMonthExpected = Math.Round(premiumCalculationsYear.PremiumAmountPerYear / 12, 2);

            //Assert
            Assert.Equal(premiumCalculationMonthExpected, premiumCalculationsYear.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH));

        }

        [Fact]
        public void PremiumCalculationsCanCalculateTheYear()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 10000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(19, "10-10-2000", 8888, 0);

            var premiumCalculationsYear = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            // Act
            var premiumCalculationYearExpected = Math.Round(premiumCalculationsYear.PremiumAmountPerYear * 0.975, 2);

            //Assert
            Assert.Equal(premiumCalculationYearExpected, premiumCalculationsYear.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR));

        }

        [Fact]
        public void PremiumCalculationLessThen6YearsNoClaim()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(90, 10000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(25, "10-10-2000", 8888, 0);

            var premiumCalculations = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            // Act
            var premiumCalculationYearExpected = Math.Round(premiumCalculations.PremiumAmountPerYear * 0.975, 2);

            //Assert
            Assert.Equal(38.02, premiumCalculations.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR));

        }
        [Fact]
        public void PremiumCalculationMoreThen20YearsNoClaim()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(90, 10000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(45, "10-10-2000", 8888, 20);

            var premiumCalculations = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            // Act
            var premiumCalculationYearExpected = Math.Round(premiumCalculations.PremiumAmountPerYear * 0.975, 2);

            //Assert
            Assert.Equal(13.31, premiumCalculations.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR));

        }

    }
}
