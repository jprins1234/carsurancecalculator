﻿using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using System;


namespace AutoVerzekeringsPremieTests
{
   public class InputValidatorTests
    {

        [Fact]
        public void CheckIfIntFalseCheck()
        {
            //Arrange
            string a = "ffdfsgv";
            //Act

            //Assert
            Assert.False(InputValidator.Checkifint(a));
        }

        [Fact]
        public void CheckIfIntTrueCheck()
        {
            //Arrange
            string a = "30";
            //Act

            //Assert
            Assert.True(InputValidator.Checkifint(a));
        }
    }
}
